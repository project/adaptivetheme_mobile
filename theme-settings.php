<?php // $Id$
// adaptivethemes.com st

/**
 * @file theme-settings.php
 */

// Include the definition of adaptivetheme_settings() and adaptivetheme_theme_get_default_settings().
include_once(drupal_get_path('theme', 'adaptivetheme') .'/theme-settings.php');

/**
* Implementation of themehook_settings() function.
*
* @param $saved_settings
*   An array of saved settings for this theme.
* @return
*   A form array.
*/
function adaptivetheme_mobile_settings($saved_settings) {

  // Get the default values from the .info file.
  $defaults = adaptivetheme_theme_get_default_settings('adaptivetheme_mobile');

  // Merge the saved variables and their default values.
  $settings = array_merge($defaults, $saved_settings);

  // Create the form using Forms API: http://api.drupal.org/api/6
  $form = array();
  // Layout settings
  if ($settings['mobile_layout_enable_settings'] == 'on') {
    $form['layout']['mobile_page_layout'] = array(
      '#type' => 'fieldset',
      '#title' => t('Mobile Page Layout'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 80,
      '#description' => t('Use these settings to customize the layout of your mobile site. NOTE: If you have the built-in Admin theme enabled these settings will not affect the Admin section; they only apply to the "front end" theme. If no overrides are set the default layout will apply.'),
    );
    if ($settings['mobile_layout_enable_method'] == 'on') {
      $form['layout']['mobile_page_layout']['mobile_layout_method_help'] = array(
        '#prefix' => '<div class="layout-help">',
        '#suffix' => '</div>',
        '#value' => t('<dl><dt>Sidebar layout</dt><dd>Set the sidebar configuration.</dd></dl>'),
      );
      $form['layout']['mobile_page_layout']['mobile_layout_method'] = array(
        '#type' => 'radios',
        '#prefix' => '<div class="layout-method">',
        '#suffix' => '</div>',
        '#default_value' => $settings['mobile_layout_method'],
        '#options' => array(
          '0' => t('<strong>Layout #1</strong> <span class="layout-type">Both sidebars below the main content, 100% width, stacked, width settings do not apply.</span>'),
          '1' => t('<strong>Layout #2</strong> <span class="layout-type">Both sidebars below the main content, side by side, width settings apply.</span>'),
          '2' => t('<strong>Layout #3</strong> <span class="layout-type">Standard three column layout, width settings apply.</span>'),
        ),
       '#attributes' => array('class' => 'layouts'),
      );
      $form['layout']['mobile_page_layout']['mobile_layout_enable_settings'] = array(
        '#type' => 'hidden',
        '#value' => $settings['mobile_layout_enable_settings'],
      );
    } // endif layout method
    if ($settings['mobile_layout_enable_sidebars'] == 'on') {
      $form['layout']['mobile_page_layout']['mobile_layout_sidebar_help'] = array(
        '#prefix' => '<div class="layout-help">',
        '#suffix' => '</div>',
        '#value' => t('<dl><dt>Sidebar widths</dt><dd>Set the width of each sidebar if using Method #2 or #3. Mobile layouts use percentage values.</dd></dl>'),
      );
      $form['layout']['mobile_page_layout']['mobile_layout_sidebar_first_width'] = array(
        '#type' => 'select',
        '#title' => t('Sidebar first'),
        '#prefix' => '<div class="sidebar-width"><div class="sidebar-width-left">',
        '#suffix' => '</div>',
        '#default_value' => $settings['mobile_layout_sidebar_first_width'],
        '#options' => array(
          '20' => t('20%'),
          '25' => t('25%'),
          '33' => t('33%'),
          '40' => t('40%'),
          '50' => t('50%'),
          '60' => t('60%'),
          '67' => t('67%'),
          '75' => t('75%'),
          '80' => t('80%'),
        ),
        '#attributes' => array('class' => 'sidebar-width-select'),
      );
      $form['layout']['mobile_page_layout']['mobile_layout_sidebar_last_width'] = array(
        '#type' => 'select',
        '#title' => t('Sidebar last'),
        '#prefix' => '<div class="sidebar-width-right">',
        '#suffix' => '</div></div>',
        '#default_value' => $settings['mobile_layout_sidebar_last_width'],
        '#options' => array(
          '20' => t('20%'),
          '25' => t('25%'),
          '33' => t('33%'),
          '40' => t('40%'),
          '50' => t('50%'),
          '60' => t('60%'),
          '67' => t('67%'),
          '75' => t('75%'),
          '80' => t('80%'),
        ),
        '#attributes' => array('class' => 'sidebar-width-select'),
      );
    } //endif layout sidebars
  } // endif layout settings
  // Access keys
  $form['access_keys'] = array(
    '#type' => 'fieldset',
    '#title' => t('Access Keys'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description'   => t('Access keys are a navigation aid for some mobile browsers.'),
  );
  $form['access_keys']['add_access_keys'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add Access Keys to Primary and Secondary links.'),
    '#default_value' => $settings['add_access_keys'],
  ); // End Access Keys
  // Color schemes
  if ($settings['color_enable_schemes'] == 'on') {
    $form['color'] = array(
      '#type' => 'fieldset',
      '#title' => t('Color settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 90,
      '#description'   => t('Use these settings to customize the colors of your site. If no stylesheet is selected the default colors will apply.'),
    );
    $form['color']['color_schemes'] = array(
      '#type' => 'select',
      '#title' => t('Color Schemes'),
      '#default_value' => $settings['color_schemes'],
      '#options' => array(
        'colors-default.css' => t('Default Color Scheme'),
      //'colors-mine.css' => t('My Color Scheme'), // add extra color css files, place these in your css/theme folder
      ),
    );
    $form['color']['color_enable_schemes'] = array(
      '#type'    => 'hidden',
      '#value'   => $settings['color_enable_schemes'],
    );
  } // endif color schemes

  // Add the base theme's settings.
  $form += adaptivetheme_settings($saved_settings, $defaults);

  // Return the form
  return $form;
}