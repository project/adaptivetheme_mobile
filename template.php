<?php // $Id$
// adaptivethemes.com st

/**
 * @file template.php
 */

// Don't include custom functions if the database is inactive.
if (db_is_active()) {
  // Include base theme custom functions.
  include_once(drupal_get_path('theme', 'adaptivetheme') .'/inc/template.custom-functions.inc');
}

/**
 * Add the color scheme stylesheet if color_enable_schemes is set to 'on'.
 * Note: you must have at minimum a color-default.css stylesheet in /css/theme/
 */
if (theme_get_setting('color_enable_schemes') == 'on') {
  drupal_add_css(drupal_get_path('theme', 'adaptivetheme_mobile') .'/css/theme/'. get_at_colors(), 'theme');
}

/**
 * USAGE
 * 1. Rename each function to match your subthemes name,
 *    e.g. if you name your theme "themeName" then the function
 *    name will be "themeName_preprocess_hook".
 * 2. Uncomment the required fucntion to use. You can delete the
 *    "sample_variable".
 */

/**
 * Override or insert variables into all templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered.
 */
/*
function adaptivetheme_mobile_preprocess(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
*/

/**
 * Override or insert variables into the page templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered.
 */
function adaptivetheme_mobile_preprocess_page(&$vars, $hook) {

  // Layout settings - set the page width and layout method.
  if (theme_get_setting('mobile_layout_enable_settings') == 'on') {

    $method = theme_get_setting('mobile_layout_method');
    $sidebar_last_width = theme_get_setting('mobile_layout_sidebar_last_width');
    $sidebar_first_width = theme_get_setting('mobile_layout_sidebar_first_width');
    if ($vars['language']->dir == 'ltr') {
      $left = 'left';
      $right = 'right';
    }
    if ($vars['language']->dir == 'rtl') {
      $left = 'right';
      $right = 'left';
    }
    if ($method == '0') {
      $styles = array();
      $styles[] = '#content-column{float:none;}';
      $styles[] = '#sidebar-first,#sidebar-last{float:none;margin:0;width:auto;}';
    }
    if ($method == '1') {
      $styles = array();
      $styles[] = '#content-column{float:none;}';
      $styles[] = '.two-sidebars .content-inner{margin-'. $left .':0; margin-'. $right .':0;}';
      $styles[] = '.sidebar-first .content-inner{margin-'. $left .':0; margin-'. $right .':0;}';
      $styles[] = '.sidebar-last .content-inner{margin-'. $right .':0; margin-'. $left .':0;}';
      $styles[] = '#sidebar-first{width:'. $sidebar_first_width .'%;margin-'. $left .':0;}';
      $styles[] = '#sidebar-last{width:'. $sidebar_last_width .'%;margin-'. $left .':0;}';
    }
    if ($method == '2') {
      $push_right = $sidebar_last_width;
      $push_left  = $sidebar_first_width;
      $pull_right = $sidebar_last_width;
      $styles = array();
      $styles[] = '.two-sidebars .content-inner{margin-'. $left .':'. $push_left .'%; margin-'. $right .':'. $push_right .'%;}';
      $styles[] = '.sidebar-first .content-inner{margin-'. $left .':'. $push_left .'%; margin-'. $right .':0;}';
      $styles[] = '.sidebar-last .content-inner{margin-'. $right .':'. $push_right .'%; margin-'. $left .':0;}';
      $styles[] = '#sidebar-first{width:'. $sidebar_first_width .'%;margin-'. $left .':-100%;}';
      $styles[] = '#sidebar-last{width:'. $sidebar_last_width .'%;margin-'. $left .':-'. $pull_right .'%;}';
    }
    $vars['at_layout'] = implode('', $styles);
    if ((theme_get_setting('at_admin_theme') == 1 && arg(0) !== 'admin') || (theme_get_setting('at_admin_theme') == 0)) {
      $vars['layout_settings'] = '<style type="text/css">'. $vars['at_layout_width'] . $vars['at_layout'] .'</style>';
    }
    if (theme_get_setting('at_admin_theme') == 1 && arg(2) == 'block') {
      $vars['layout_settings'] = '<style type="text/css">'. $vars['at_layout'] .'</style>';
    }
  }

  $classes = explode(' ', $vars['classes']);
  // Add the admin-menu class manually since JavaScript is not availble in the mobile theme.
  if (module_exists('admin_menu')) {
    $classes[] = 'admin-menu';
  }
  // $classes is the varaible that will hold the page classes, and is printed in page tpl files.
  $vars['classes'] = implode(' ', $classes);
  
  // Add mobile meta tags to $head
  $head = array($vars['head']);
  $head[] = '<meta name="HandheldFriendly" content="True" />' . "\n";
  $head[] = '<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />' . "\n";
  if (!module_exists('touch_icons')) {
    $head[] = '<link rel="apple-touch-icon" href="'. base_path() . drupal_get_path('theme', 'adaptivetheme_mobile') .'/apple-touch-icon.png'.'"/>' . "\n";
  }
  $vars['head'] = implode('', $head);

  // Unset adaptivetheme core CSS files, AT Mobile uses its own.
  $css = drupal_add_css();
  $core_styles = array('base.css');
  $path_to_core = drupal_get_path('theme', 'adaptivetheme') .'/css/core/';
  foreach ($core_styles as $stylesheet) {
    $files = $path_to_core . $stylesheet;
    unset($css['all']['theme'][$files]);
  }
  $vars['styles'] = drupal_get_css($css);

}

/**
 * Override or insert variables into the node templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered.
 */
/*
function adaptivetheme_mobile_preprocess_node(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
*/

/**
 * Override or insert variables into the comment templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered.
 */
/*
function adaptivetheme_mobile_preprocess_comment(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
*/

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered.
 */
/*
function adaptivetheme_mobile_preprocess_block(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
*/
