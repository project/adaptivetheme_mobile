<?php // $Id$
// adaptivethemes.com

/**
* @file maintenance-page.tpl.php
*
* Theme implementation to display a single Drupal page while off-line.
*
* @see template_preprocess()
* @see template_preprocess_maintenance_page()
*/
?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>      
<body class="<?php print $body_classes; ?>">
  <div id="container">
    <div id="header" class="clearfix">
      <?php if ($site_logo or $site_name or $site_slogan): ?>
        <div id="branding">
          <?php if (!empty($logo)): ?>
            <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
              <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
            </a>
          <?php endif; ?>
          <?php if (!empty($site_name)): ?>
            <div id="site-name"><strong>
              <a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
            </strong></div>
          <?php endif; ?>
          <?php if ($site_slogan): ?>
            <div id="site-slogan"><?php print $site_slogan; ?></div>
          <?php endif; ?>
        </div>
      <?php endif; ?>
    </div>
    <div id="columns"><div class="columns-inner clearfix">
      <div id="content-column"><div class="content-inner">
        <div id="main-content">
          <?php if ($title): ?><h1 id="page-title"><?php print $title; ?></h1><?php endif; ?>
          <div id="content"><?php print $content; ?></div>
        </div>
      </div></div>
    </div></div>
  </div>
  <?php print $closure ?>
</body>
</html>