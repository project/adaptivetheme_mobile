<?php // $Id$
// adaptivethemes.com
?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $layout_settings; ?>
</head>
<body class="<?php print $classes; ?>">
  <div id="container">
    <div id="skip-nav" class="<?php print $skip_nav_class; ?>">
      <h2 class="element-invisible"><?php print t('Skip menu'); ?></h2>
      <ul>
        <li><a href="#main-content"><?php print t('Content'); ?></a></li>
        <?php if (!empty($primary_menu) or !empty($secondary_menu)): ?>
          <li><a href="#navigation"><?php print t('Menu'); ?></a></li>
        <?php endif; ?>
        <?php if ($search_box): ?>
          <li><a href="#search-box"><?php print t('Search'); ?></a></li>
        <?php endif; ?>
      </ul>
    </div>
    <?php if ($messages or $help): ?>
      <div id="messages-and-help">
        <h2 class="element-invisible"><?php print t('System Messages'); ?></h2>
        <?php if ($messages): print $messages; endif; ?>
        <?php if ($help): print $help; endif; ?>
      </div>
    <?php endif; ?>
    <div id="header" class="clearfix">
      <?php if ($linked_site_logo or $linked_site_name or $site_slogan): ?>
        <div id="branding">
          <?php if ($linked_site_logo or $linked_site_name): ?>
            <?php if ($title): ?>
              <div class="logo-site-name"><strong>
                <?php if ($linked_site_logo): ?><span id="logo"><?php print $linked_site_logo; ?></span><?php endif; ?>
                <?php if ($linked_site_name): ?><span id="site-name"><?php print $linked_site_name; ?></span><?php endif; ?>
              </strong></div>
            <?php else: /* Use h1 when the content title is empty */ ?>
              <h1 class="logo-site-name">
                <?php if ($linked_site_logo): ?><span id="logo"><?php print $linked_site_logo; ?></span><?php endif; ?>
                <?php if ($linked_site_name): ?><span id="site-name"><?php print $linked_site_name; ?></span><?php endif; ?>
             </h1>
            <?php endif; ?>
          <?php endif; ?>
          <?php if ($site_slogan): ?>
            <div id="site-slogan"><?php print $site_slogan; ?></div>
          <?php endif; ?>
        </div>
      <?php endif; ?>
      <?php if ($breadcrumb): ?>
        <div id="breadcrumb">
          <h2 class="element-invisible"><?php print t('You are here:'); ?></h2>
          <?php print $breadcrumb; ?>
        </div>
      <?php endif; ?>
      <?php if ($header): ?>
        <div id="header-region"><?php print $header; ?></div>
      <?php endif; ?>
    </div>
    <div id="columns"><div class="columns-inner clearfix">
      <div id="content-column"><div class="content-inner">
        <?php if ($mission): ?>
          <div id="mission"><?php print $mission; ?></div>
        <?php endif; ?>
        <div id="main-content">
          <?php if ($title or $tabs): ?>
            <div id="main-content-header">
              <?php if ($title): ?><h1 id="page-title"><?php print $title; ?></h1><?php endif; ?>
              <?php if ($tabs): ?><div class="local-tasks"><?php print $tabs; ?></div><?php endif; ?>
            </div>
          <?php endif; ?>
          <div id="content"><?php print $content; ?></div>
        </div>
      </div></div>
      <?php if ($left): ?>
        <div id="sidebar-first" class="sidebar"><?php print $left; ?></div>
      <?php endif; ?>
      <?php if ($right): ?>
        <div id="sidebar-last" class="sidebar"><?php print $right; ?></div>
      <?php endif; ?>
    </div></div>
    <?php if (!empty($primary_menu) or !empty($secondary_menu)): ?>
      <div id="navigation">
        <?php if (!empty($primary_menu)): ?>
          <div id="primary" class="nav">
            <h2 class="element-invisible"><?php print t('Main Menu'); ?></h2>
            <?php print $primary_menu; ?>
          </div>
        <?php endif; ?>
        <?php if (!empty($secondary_menu)): ?>
          <div id="secondary" class="nav">
            <h2 class="element-invisible"><?php print t('Secondary Menu'); ?></h2>
            <?php print $secondary_menu; ?>
          </div>
        <?php endif; ?>
      </div>
    <?php endif; ?>
    <?php if ($search_box): ?>
      <div id="search-box"<?php print $toggle_label ?>><?php print $search_box; ?></div>
    <?php endif; ?>
    <?php if ($footer or $footer_message): ?>
      <div id="footer">
        <?php if ($footer): ?>
          <div id="footer-region"><?php print $footer; ?></div>
        <?php endif; ?>
        <?php if ($footer_message): ?>
          <div id="footer-message"><?php print $footer_message; ?></div>
        <?php endif; ?>
      </div>
    <?php endif; ?>
  </div>
  <div id="top-link"><a href="#container"><?php print t('Top'); ?></a></div>
  <?php print $closure ?>
</body>
</html>